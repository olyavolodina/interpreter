

#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include "type_of_lex.h"
#include "Lex.h"
using namespace std;

#ifndef Scanner_h
#define Scanner_h
extern tabl_ident TID;

class Scanner{
public :
	static const char * TW[];
	static const char * TD[];
	static int rows ;
	static int cols;
private:
	enum state { H, IDENT, NUMB, ALE, DELIM, NEQ, REAL, MINUS, PLUS};
	static type_of_lex words[];
	static type_of_lex dlms[];
	state CS;
	FILE * fp;
	char c ;
	char buf[80];
	int buf_top;
	void clear (){

		buf_top = 0;
		for ( int j = 0; j < 80; ++j )
			buf[j] = '\0';
	}
	void add (){
		buf [ buf_top ++ ] = c;
	}
	int look ( const char *buf, const char **list ){
		int i = 0;
		while ( list[i] ){
			if ( !strcmp(buf, list[i]) )
			return i;
			++i;
		}
		return 0;
	}
	void gc (){
		if (c != '\t')
			cols ++;
		else 
			cols += 4;
		c = fgetc (fp);
	}

public :
	Lex get_lex ();
	Scanner ( const char * program ){
		fp = fopen ( program, "r" );
		CS = H;
		buf_top = 0;
		clear();
		gc();

	}
};


#endif
