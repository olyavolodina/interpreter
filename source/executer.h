#include <cstdio>

class Executer{
	Lex pc_el;
public :
void execute ( Poliz& prog );
};

void Executer::execute ( Poliz& prog ){
	Stack < float , 100 > args;
	float fl;
	int i, j,  index = 0, size = prog.get_free();
	while ( index < size ){
		//printf("%d ",index);
		pc_el = prog [ index ];

		switch ( pc_el.get_type () ){
		case LEX_TRUE:
		case LEX_FALSE:
		case LEX_NUM:
		case LEX_NUMFLOAT:
		case POLIZ_ADDRESS:
		case POLIZ_LABEL:

			args.push ( pc_el.get_value () );
			break ;
		case LEX_ID:
			i = pc_el.get_value ();
			args.push ( TID[i].get_value () );

			break;
		case LEX_NOT:
			args.push( !args.pop() );
			break ;
		case LEX_OR:
			i = args.pop();
			args.push ( args.pop() || i );
			break ;
		case LEX_PSPLUS:
			j = args.pop();
			args.push(TID[j].get_value());
			TID[j].put_value(TID[j].get_value()+1);
			break;
		case LEX_PSMIN:
			j = args.pop();
			args.push(TID[j].get_value());
			TID[j].put_value(TID[j].get_value()-1);
			break;
		case LEX_PRMIN:
			j = args.pop();
			TID[j].put_value(TID[j].get_value()-1);
			args.push(TID[j].get_value());
			break;
		case LEX_PRPLUS:
			j = args.pop();
			TID[j].put_value(TID[j].get_value()+1);
			args.push(TID[j].get_value());
			break;		
		case LEX_MOD:
			i = args.pop();
			if (i != 0){
				args.push(int(args.pop()) % int(i));

				break;
			} else {
				throw runtime_error(string("division by zero"));
			}
		case LEX_AND:
			i = args.pop();
			args.push ( args.pop() && i );
			break ;
		case POLIZ_GO:
			index = args.pop() - 1;
			break ;
		case POLIZ_FGO:
			i = args.pop();
			if ( !args.pop() )
		 
				index = i - 1;
			break ;
		case LEX_WRITE:
			cout << args.pop () << endl;
			break ;
		case LEX_READ: {
			int k;
			float f;
			i = args.pop ();
			if ( TID[i].get_type () == LEX_INT ) {
				cout << "Input int value for ";
				cout << TID[i].get_name () << endl;
				cin >> k;
			} else  if ( TID[i].get_type () == LEX_BOOL ){
				char j[20];
				rep:
				cout << "Input boolean value (true or false) for ";
				cout << TID[i].get_name() << endl;
				cin >> j;
				if ( !strcmp(j, "true") )
					k = 1;
				else if ( !strcmp(j, "false") )
					k = 0;
				else {
					cout << "Error in input:true/false";
					cout << endl;
					goto rep;
				}
			} else {
				cout << "Input float value for ";
				cout << TID[i].get_name () << endl;
				cin >> f;

				TID[i].put_value (f);

				break;
			}
			TID[i].put_value (k);

			break ;
		}
		case LEX_PLUS:
			args.push ( args.pop() + args.pop() );
			break ;
		case LEX_TIMES:
			args.push ( args.pop() * args.pop() );
			break ;
		case LEX_MINUS:
			i = args.pop();
			args.push ( args.pop() - i );
			break ;
		case LEX_SLASH:
			i = args.pop();
			if ( i!=0 ) {
				args.push ( int (args.pop() / i) );
				break ;
			} else
				 throw runtime_error(string("POLIZ:division by zero"));
		case LEX_FSLASH:
			fl = args.pop();
			if ( i!=0 ) {
				args.push ( args.pop() / fl) ;
				break ;
			} else
				 throw runtime_error(string("POLIZ:division by zero"));
		case LEX_EQ:

			args.push ( args.pop() == args.pop() );
			break ;
		case LEX_LSS:
			i = args.pop();
			args.push ( args.pop() < i);
			break ;
		case LEX_GTR:
			i = args.pop();
			args.push ( args.pop() > i );
			break ;
		case LEX_LEQ:
			i = args.pop();
			args.push ( args.pop() <= i );
			break ;
		case LEX_GEQ:
			i = args.pop();
			args.push ( args.pop() >= i );
			break ;
		case LEX_NEQ:
			i = args.pop();
			args.push ( args.pop() != i );
			break ;
		case LEX_ASSIGN:
			fl = args.pop();
			j = args.pop();
			TID[j].put_value(fl);

			break ;
		default :
			throw runtime_error(string("POLIZ: unexpected elem"));
	} // end of switch
	++index;
	}; //end of while

}




