#include <exception>
using namespace std;


class Err : public exception {
protected:
	int cols, rows;
	virtual void  print (ostream &s)  = 0;
public:
	Err ():cols(Scanner::cols), rows (Scanner :: rows){}
	~Err () throw() {};

	friend ostream& operator << ( ostream &s,  Err &er ){
		er.print(s);
		return(s);
	}
};


class SyntaxErr: public Err{
	Lex curr;
public:
	SyntaxErr(Lex c ):curr(c){};
	~SyntaxErr() throw() {};
protected:
	void print(ostream &s) {
		s<<  curr << " in (" << rows << "," << cols << ")"; 
	}


};

class LexErr: public Err{
	char c;
public:
	LexErr ( char e): c(e){};
	~LexErr() throw () {};
protected:
	void print(ostream &s) {
		s<< "'"<<  c <<"'" << " in (" << rows << "," << cols << ")"; 
	}
};


class SemErr: public Err{
	string str;
public:
	SemErr( string s):str(s){};
	~SemErr() throw() {};
protected:
	void print(ostream &s) {
		s<<  str << " in (" << rows << "," << cols << ")"; 
	}
};