
#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include "scanner.h"
#include "type_of_lex.h"
#include "Lex.h"
#include "tabl_ident.h"
#include "exception.h"


int Scanner :: rows  = 1; 
int Scanner :: cols = 0;
Lex Scanner::get_lex () {
	int d, j, fr = 1;
	float f;
	CS = H;

	do {
		switch ( CS ){
		case H:
			if ( c ==' ' || c =='\n' || c=='\r' || c =='\t' ){
				if (c == '\n'){
					rows++;
					cols = 0;
				}
				gc ();
			} else if ( isalpha(c) ){
				clear ();
				add ();
				gc ();
				CS = IDENT;
				}
			else if ( isdigit(c) ){

				d = c - '0';
				gc ();
				CS = NUMB;
				}
			else if ( c== '{' ){
				gc();
				return Lex(LEX_LFIGPAR);
				
				}
			else if ( c== '}' ){
				gc();
				return Lex(LEX_RFIGPAR);
				
				}
			else if (c == '-'){
				clear();
				add();
				gc();
				CS = MINUS;
			}
			else if (c == '+'){
				clear();
				add();
				gc();
				CS = PLUS;
			}
			else if ( c== '=' || c== '<' || c== '>'){
				clear ();
				add ();
				gc ();
				CS = ALE;
				}
			else if ( c == -1 )
				return Lex(LEX_FIN);
			else if (c =='-'){
				gc();

			}

			else if ( c == '!' ){
				clear ();
				add ();
				gc ();
				CS = NEQ;
				}
			else
				CS = DELIM;
			break ;
		case IDENT:
			if ( isalpha(c) || isdigit(c) ){
				add ();
				gc ();
			}
			else
				if ( j = look (buf, TW) )
					return Lex (words[j], j);
				else {
					j = TID.put(buf);
					return Lex (LEX_ID, j);
				}
			break ;
		case NUMB:
			if ( isdigit(c) ){
				d = d * 10 + (c - '0');
				gc();
			}
			else if (c == '.'){
				CS = REAL;
				f = float(d);
				gc();
			}
			else {
				return Lex ( LEX_NUM, d );
			}
			break ;
		case REAL:
			if ( isdigit(c)){
				fr *= 10;
				f = f+ (c-'0')/(float)fr;
				cout<< f << endl;

				gc();

			}
			else {
				return Lex ( LEX_NUMFLOAT, f);
			}
			break;
		case MINUS:
			if (c == '-'){
				add();
				gc();
				j = look(buf, TD );
				return Lex(dlms[j], j);
			} else {
				j = look(buf, TD);
				return Lex(dlms[j], j);
			}
		case PLUS:
			if (c == '+'){
				add();
				gc();
				j = look(buf, TD );
				return Lex(dlms[j], j);
			} else {
				j = look(buf, TD);
				return Lex(dlms[j], j);
			}
		case ALE:
			if ( c == '=' ){
				add ();
				gc ();
				
				j = look ( buf, TD );
				return Lex ( dlms[j], j );
			}
			else {
				j = look (buf, TD);
				return Lex ( dlms[j], j );
			}
			break ;
		case NEQ:
			if ( c == '=' ){
				add ();
				gc ();
				j = look ( buf, TD );
				return Lex ( LEX_NEQ, j );
			}
			else
				throw LexErr('!');
			break ;
		case DELIM:
			clear ();
			add ();
			if ( j = look(buf, TD) ){
				gc ();
				return Lex ( dlms[j], j );
			}
			else if (c == -1){
				
				return Lex(LEX_FIN, 0);
			}
			else {
				throw LexErr(c);
			}
			break ;
		} // end switch
	} while ( true );
}

const char * Scanner::TW[] =
{
"", 
"and", 
"bool", 
"else", 
"if", 
"false", 
"int", 
"not", 
"or", 
"program", 
"read", 
"true", 
"var", 
"while", 
"write", 
"for",  
"continue", 
"break", 
"float", 
NULL
};
const char * Scanner:: TD[] =
{
"", 
";", 
",", 
":", 
"=", 
"(", 
")", 
"==", 
"<", 
">", 
"+", 
"-", 
"*", 
"/", 
"<=", 
"!=", 
">=", 
"--", 
"++", 
"%",
"{",
"}",
NULL
};


type_of_lex Scanner::words[] =
{
LEX_NULL,
LEX_AND,
LEX_BOOL,
LEX_ELSE,
LEX_IF,
LEX_FALSE,
LEX_INT,
LEX_NOT,
LEX_OR,
LEX_PROGRAM,
LEX_READ,
LEX_TRUE,
LEX_VAR,
LEX_WHILE,
LEX_WRITE,
LEX_FOR,
LEX_CONTIN,
LEX_BREAK,
LEX_FLOAT,
LEX_NULL
};
 type_of_lex Scanner::dlms[] =
{
LEX_NULL,
LEX_SEMICOLON,
LEX_COMMA,
LEX_COLON,
LEX_ASSIGN,
LEX_LPAREN,
LEX_RPAREN,
LEX_EQ,
LEX_LSS,
LEX_GTR,
LEX_PLUS,
LEX_MINUS,
LEX_TIMES,
LEX_SLASH,
LEX_LEQ,
LEX_NEQ,
LEX_GEQ,
LEX_UNMIN,
LEX_UNPLUS,
LEX_MOD,
LEX_LFIGPAR,
LEX_RFIGPAR,
LEX_NULL
};
