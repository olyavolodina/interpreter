#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
using namespace std;

#ifndef list_h
#define list_h

class list{
	struct Node{
		int elem;
		struct Node * next;
	} * lst , *firstnode;
public:
	list(){
		lst = firstnode = NULL;
	}
	list (list &);
	~list();
	void add(int);
	int get();
	bool empty() const;
	void print();
	void operator = ( list &);
};

#endif