#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include "list.h"
using namespace std;


void list::add(int e){
	if (lst == NULL) {
		firstnode = lst = new struct Node;
		lst->next = NULL;
	} else {
		lst = new struct Node;
		lst->next = firstnode;
		firstnode = lst;
	}
	lst->elem = e;


}

int list::get(){
	int res;
	if (firstnode == NULL){
		return(-1);
	}
	lst = firstnode;
	firstnode = firstnode->next;
	res = lst->elem;

	delete lst;
	lst = firstnode;
	return(res);
}


list::~list(){
	while (firstnode != NULL){
		lst = firstnode;
		firstnode = firstnode->next;
		delete lst;
	}
}


bool list::empty() const{
	if (firstnode == NULL){
		return(1);
	} else {
		return(0);
	}
}

void list::print(){
	if (firstnode !=NULL){
		lst = firstnode;
		while (lst != NULL){

			printf("%d\n", lst->elem);
			lst = lst->next;
		}
	}
	lst = firstnode;
}

list::list (list & obj){
	while (obj.empty() != 1){
		this->add(obj.get());
			
	}
}

void list::operator = ( list & obj){
	while (obj.empty() != 1){
		this->add(obj.get());
	}
}
			