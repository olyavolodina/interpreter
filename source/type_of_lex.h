#ifndef type_h
#define type_h

enum type_of_lex
{
LEX_FSLASH,
LEX_NUMFLOAT,
LEX_CONTIN,

LEX_BREAK,
LEX_ID, //0
LEX_NULL,
LEX_NUM,
LEX_AND,

LEX_BOOL,
LEX_FLOAT,

LEX_ELSE,

LEX_IF,
LEX_FALSE,
LEX_INT,
LEX_NOT,
LEX_OR,
LEX_PROGRAM,
LEX_READ,
LEX_FOR,

LEX_TRUE,
LEX_VAR,
LEX_WHILE,
LEX_WRITE,
///////////////////////////////////
LEX_FIN,
LEX_SEMICOLON,
LEX_COMMA,
LEX_COLON,
LEX_ASSIGN,
LEX_LPAREN,
LEX_RPAREN,
LEX_EQ,
LEX_LSS,
LEX_GTR,
LEX_PLUS,
LEX_MINUS,
LEX_TIMES,
LEX_SLASH,
LEX_LEQ,
LEX_NEQ,
LEX_GEQ, // 41
LEX_UNMIN,
LEX_UNPLUS,
LEX_MOD,
LEX_LFIGPAR,
LEX_RFIGPAR,
LEX_PSPLUS,
LEX_PSMIN,
LEX_PRMIN,
LEX_PRPLUS,
POLIZ_GO,
POLIZ_FGO,
POLIZ_LABEL,
POLIZ_ADDRESS,
};

#endif