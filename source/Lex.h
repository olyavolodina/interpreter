#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include "type_of_lex.h"
#include "tabl_ident.h"
using namespace std;
#ifndef Lex_h
#define Lex_h
extern tabl_ident TID;

class Lex{
	static const char * TW[]; 
	type_of_lex t_lex;
	float v_lex;
public :
	Lex ( type_of_lex t = LEX_NULL, float v = 0){
		t_lex = t; v_lex = v;
	}
	type_of_lex get_type () { return t_lex; }
	float get_value () { return v_lex; }
	friend ostream& operator << ( ostream &s, Lex l ){

		if (l.t_lex == POLIZ_LABEL){
			
			s << '(' << TW[l.t_lex] << " , " << l.v_lex << ')' ;
		} else if (l.t_lex == POLIZ_ADDRESS){
			s<< '(' << TW[l.t_lex] << " , " << (TID[l.v_lex]).get_name() << ')';
		} else if (l.t_lex == LEX_NUM){
			s << '(' << l.v_lex << ')';
		} else if (l.t_lex == LEX_NUMFLOAT){
			s << '(' << l.v_lex << ')';
		

		} else if (l.t_lex != LEX_ID){
			s << '(' << TW[l.t_lex] << ")";
		} else
			s << '(' << (TID[l.v_lex]).get_name() << ")";

			

		return s;
	}
};


#endif