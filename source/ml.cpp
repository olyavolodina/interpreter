#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
using namespace std;


enum type_of_lex
{
LEX_CONTIN,
LEX_GOTO,
LEX_BREAK,
LEX_ID, //0
LEX_NULL,
LEX_NUM,
LEX_AND,

LEX_BOOL,
LEX_FLOAT,

LEX_ELSE,

LEX_IF,
LEX_FALSE,
LEX_INT,
LEX_NOT,
LEX_OR,
LEX_PROGRAM,
LEX_READ,
LEX_FOR,

LEX_TRUE,
LEX_VAR,
LEX_WHILE,
LEX_WRITE,
///////////////////////////////////
LEX_FIN,
LEX_SEMICOLON,
LEX_COMMA,
LEX_COLON,
LEX_ASSIGN,
LEX_LPAREN,
LEX_RPAREN,
LEX_EQ,
LEX_LSS,
LEX_GTR,
LEX_PLUS,
LEX_MINUS,
LEX_TIMES,
LEX_SLASH,
LEX_LEQ,
LEX_NEQ,
LEX_GEQ, // 41
LEX_UNMIN,
LEX_UNPLUS,
LEX_MOD,
LEX_LFIGPAR,
LEX_RFIGPAR,
};

class ident{
	char * name;
	bool declare;
	type_of_lex type;
	bool assign;
	float value;
public :
	ident (){
		declare = false ;
		assign = false ;
	}
	char *get_name (){
 		return name;
	}
	void put_name ( const char *n){
		name = new char [ strlen(n) + 1 ];
		strcpy ( name, n );
	}
	bool get_declare (){
		return declare;
	}
	void put_declare (){
		declare = true;
	}
	type_of_lex get_type (){
		return type;
	}
	void put_type ( type_of_lex t ){
		type = t;
	}
	bool get_assign (){
		return assign;
	}
	void put_assign (){
		assign = true ;
	}
	int get_value (){
		return value;
	}
	void put_value ( float v){
		value = v;
	}
};

class tabl_ident{
	ident * p;
	int size;
	int top;
public :
	tabl_ident ( int max_size ){
		p = new ident[size=max_size];
		top = 1;
	}
	~tabl_ident (){
		delete []p;
	}
	ident & operator [] ( int k ){
		return p[k];
	}
	int put ( const char *buf );
};
 tabl_ident TID(100);
class Lex
{
	static const char * TW[]; 
	type_of_lex t_lex;
	float v_lex;
public :
	Lex ( type_of_lex t = LEX_NULL, float v = 0){
		t_lex = t; v_lex = v;
	}
	type_of_lex get_type () { return t_lex; }
	float get_value () { return v_lex; }
	friend ostream& operator << ( ostream &s, Lex l ){
		if (l.t_lex != LEX_ID)
			cout << '(' << TW[l.t_lex] << ");";
		else
			cout << '(' << (TID[l.v_lex]).get_name() << ");";

			

		return s;
	}
};
const char * Lex::TW[] =
{
"LEX_CONTIN",
"LEX_GOTO",
"LEX_BREAK",
"LEX_ID", //0
"LEX_NULL",
"LEX_NUM",
"LEX_AND",

"LEX_BOOL",
"LEX_FLOAT",

"LEX_ELSE",

"LEX_IF",
"LEX_FALSE",
"LEX_INT",
"LEX_NOT",
"LEX_OR",
"LEX_PROGRAM",
"LEX_READ",
"LEX_FOR",

"LEX_TRUE",
"LEX_VAR",
"LEX_WHILE",
"LEX_WRITE",
///////////////////////////////////
"LEX_FIN",
"LEX_SEMICOLON",
"LEX_COMMA",
"LEX_COLON",
"LEX_ASSIGN",
"LEX_LPAREN",
"LEX_RPAREN",
"LEX_EQ",
"LEX_LSS",
"LEX_GTR",
"LEX_PLUS",
"LEX_MINUS",
"LEX_TIMES",
"LEX_SLASH",
"LEX_LEQ",
"LEX_NEQ",
"LEX_GEQ",
"LEX_UNMIN",
"LEX_UNPLUS",
"LEX_MOD",
"LEX_LFIGPAR",
"LEX_RFIGPAR", // 41
};









int tabl_ident::put ( const char *buf ){
	for ( int j=1; j<top; ++j )
		if ( !strcmp(buf, p[j].get_name()) )
			return j;
	p[top].put_name(buf);
	++top;
	return top-1;
}


class Scanner{
public :
	static const char * TW[];
	static const char * TD[];
private:
	enum state { H, IDENT, NUMB, COM, ALE, DELIM, NEQ, REAL, MINUS, PLUS};
	static type_of_lex words[];
	static type_of_lex dlms[];
	state CS;
	FILE * fp;
	char c ;
	char buf[80];
	int buf_top;
	void clear (){

		buf_top = 0;
		for ( int j = 0; j < 80; ++j )
			buf[j] = '\0';
	}
	void add (){
		buf [ buf_top ++ ] = c;
	}
	int look ( const char *buf, const char **list ){
		int i = 0;
		while ( list[i] ){
			if ( !strcmp(buf, list[i]) )
			return i;
			++i;
		}
		return 0;
	}
	void gc (){
		c = fgetc (fp);


	}
	public :

	Lex get_lex ();
	Scanner ( const char * program ){
		fp = fopen ( program, "r" );

		CS = H;
		buf_top = 0;
		clear();
		gc();

	}
};
typedef struct Node *link;


typedef struct Node {
		Lex elem;
		link next ;
		}	node;


const char * Scanner::TW[] =
{
"", // 0 позиция 0 не используется
"and", // 1

"bool", // 3

"else", // 5

"if", // 7
"false", // 8
"int", // 9
"not", // 10
"or", // 11
"program", // 12
"read", // 13

"true", // 15
"var", // 16
"while", // 17
"write", // 18
"for", //19
"goto", //20
"continue", //21
"break", //22
"float", //23
NULL
};
const char * Scanner:: TD[] =
{
"", // 0 позиция 0 не используется

"@", // 1
";", // 2
",", // 3
":", // 4
"=", // 5
"(", // 6
")", // 7
"==", // 8
"<", // 9
">", // 10
"+", // 11
"-", // 12
"*", // 13
"/", // 14
"<=", // 15
"!=", // 16
">=", // 17
"--", 
"++", 
"%",
"{",
"}",
NULL
};


type_of_lex Scanner::words[] =
{
LEX_NULL,

LEX_AND,

LEX_BOOL,

LEX_ELSE,

LEX_IF,
LEX_FALSE,
LEX_INT,
LEX_NOT,
LEX_OR,
LEX_PROGRAM,
LEX_READ,

LEX_TRUE,
LEX_VAR,
LEX_WHILE,
LEX_WRITE,
LEX_FOR,
LEX_GOTO,
LEX_CONTIN,
LEX_BREAK,
LEX_FLOAT,
LEX_NULL
};
 type_of_lex Scanner::dlms[] =
{
LEX_NULL,
LEX_FIN,
LEX_SEMICOLON,
LEX_COMMA,
LEX_COLON,
LEX_ASSIGN,
LEX_LPAREN,
LEX_RPAREN,
LEX_EQ,
LEX_LSS,
LEX_GTR,
LEX_PLUS,
LEX_MINUS,
LEX_TIMES,
LEX_SLASH,
LEX_LEQ,
LEX_NEQ,
LEX_GEQ,
LEX_UNMIN,
LEX_UNPLUS,
LEX_MOD,
LEX_LFIGPAR,
LEX_RFIGPAR,
LEX_NULL
};


Lex Scanner::get_lex () {
	int d, j, fr = 1;
	float f;
	CS = H;

	do {
		switch ( CS ){
		case H:
			if ( c ==' ' || c =='\n' || c=='\r' || c =='\t' )
				gc ();
			else if ( isalpha(c) ){
				clear ();
				add ();
				gc ();
				CS = IDENT;
				}
			else if ( isdigit(c) ){

				d = c - '0';
				gc ();
				CS = NUMB;
				}
			else if ( c== '{' ){
				gc();
				return Lex(LEX_LFIGPAR);
				
				}
			else if ( c== '}' ){
				gc();
				return Lex(LEX_RFIGPAR);
				
				}
			else if (c == '-'){
				clear();
				add();
				gc();
				CS = MINUS;
			}
			else if (c == '+'){
				clear();
				add();
				gc();
				CS = PLUS;
			}
			else if ( c== '=' || c== '<' || c== '>'){
				clear ();
				add ();
				gc ();
				CS = ALE;
				}
			else if ( c == '@' )
				return Lex(LEX_FIN);
			else if (c =='-'){
				gc();

			}

			else if ( c == '!' ){
				clear ();
				add ();
				gc ();
				CS = NEQ;
				}
			else
				CS = DELIM;
			break ;
		case IDENT:
			if ( isalpha(c) || isdigit(c) ){
				add ();
				gc ();
			}
			else
				if ( j = look (buf, TW) )
					return Lex (words[j], j);
				else {
					j = TID.put(buf);
					return Lex (LEX_ID, j);
				}
			break ;
		case NUMB:
			if ( isdigit(c) ){
				d = d * 10 + (c - '0');
				gc();
			}
			else if (c == '.'){
				CS = REAL;
				f = d;
				gc();
			}
			else {
				return Lex ( LEX_NUM, d );
			}
			break ;
		case REAL:
			if ( isdigit(c) ){
				fr *= 10;
				f = f+ (c-'0')/(float)fr;

			}
			else {

				return Lex ( LEX_FLOAT, f);
			}
		case MINUS:
			if (c == '-'){
				add();
				gc();
				j = look(buf, TD );
				return Lex(dlms[j], j);
			} else {
				j = look(buf, TD);
				return Lex(dlms[j], j);
			}
		case PLUS:
			if (c == '+'){
				add();
				gc();
				j = look(buf, TD );
				return Lex(dlms[j], j);
			} else {
				j = look(buf, TD);
				return Lex(dlms[j], j);
			}
		case ALE:
			if ( c == '=' ){
				add ();
				gc ();
				//printf("%c??????\n", c);
				j = look ( buf, TD );
				return Lex ( dlms[j], j );
			}
			else {
				j = look (buf, TD);
				return Lex ( dlms[j], j );
			}
			break ;
		case NEQ:
			if ( c == '=' ){
				add ();
				gc ();
				j = look ( buf, TD );
				return Lex ( LEX_NEQ, j );
			}
			else
				throw '!';
			break ;
		case DELIM:
			clear ();
			add ();
			if ( j = look(buf, TD) ){
				gc ();
				return Lex ( dlms[j], j );
			}
			else if (c == -1){
				//printf("AAAAAAAAAAAAAA\n");
				return Lex(LEX_FIN, 0);
			}
			else {
				throw(c);
			}
			break ;
		} // end switch
	} while ( true );
}

int main(){

	link list = NULL, firstnode = NULL;
	Scanner S("prog.txt");

	try{
		while(1){
			if (list == NULL){
				firstnode = list = new Node;
			} 
			else {
				list->next = new Node;
				list = list->next;

			}
			list->elem = S.get_lex();
					cout<<list->elem<<' '<<endl;;
			list->next = NULL;
			if ((list->elem).get_type() == LEX_FIN){
				break;
			}
		}
	}
	catch (char c){
		printf("%c", c);

	}
}
