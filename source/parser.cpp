#include <iostream>
#include <cstdio>
#include <cstring>
#include <string>
#include <cstdlib>
#include "parser.h"
#include "exception.h"
#include "Poliz.h"

using namespace std;


void Parser::addpoliz_post_plus_minus(){
	if (c_type == LEX_UNPLUS){
		prog.put_lex(Lex(LEX_PSPLUS));
	} else {
		prog.put_lex(Lex(LEX_PSMIN));
	}
}

void Parser::addpoliz_pred_minus_plus(){
	if (c_type == LEX_UNPLUS){
		prog.put_lex(Lex(LEX_PRPLUS));
	} else {
		prog.put_lex(Lex(LEX_PRMIN));
	}
}
void Parser::check_id_in_read (){
	if ( TID [c_val].get_declare() == 0 )
		throw SemErr ("undeclared");
	TID[c_val].put_assign();

}


void Parser::eq_type (){
	type_of_lex t1 = st_lex.pop() , t2 = st_lex.pop() ; 
	if (t2 == LEX_FLOAT && t1 == LEX_INT){
		return;
	}
	if (t2 != t1) 
		throw SemErr ("wrong types in assignement");
}


void Parser::eq_bool (){
	if ( st_lex.pop() != LEX_BOOL ) 
		throw SemErr("wrong types in conditions");
}
void  Parser::check_id(int asg = 0){
	if (TID[c_val].get_declare() == 0){
		throw SemErr ("undeclared");
	} else if (asg == 0 && TID[c_val].get_assign() == false) {
		throw SemErr ("not assigned");



	 } else {
	 	if (asg == 1 && TID[c_val].get_assign() == false) {
	 		TID[c_val].put_assign();
	 	}
		st_lex.push(TID[c_val].get_type());
	}
}

void Parser::check_not (){
	if (st_lex.pop() != LEX_BOOL)
		throw SemErr("wrong type in not");
	else {
		st_lex.push (LEX_BOOL);
	}
	prog.put_lex(Lex(LEX_NOT));
}
void Parser::check_plus_minus (){
	type_of_lex t;
	if ((t = st_lex.pop()) != LEX_FLOAT && (t != LEX_INT))
		throw SemErr("wrong type in unary minus/plus");
	else {
		st_lex.push (t);
	}
}

void Parser::check_op ()
{
	type_of_lex t1, t2, op, t = LEX_BOOL;
	t2 = st_lex.pop();
	op = st_lex.pop();
	t1 = st_lex.pop();
	if (t1 == LEX_FLOAT && t2 == LEX_INT || t2 == LEX_FLOAT && t1 == LEX_INT || t1 == t2 && t1 == LEX_FLOAT){
		t = LEX_FLOAT;
	} else if (t1 == LEX_INT && t2 == LEX_INT){
		t = LEX_INT;
	} else if ((t1 == LEX_BOOL && t2 != LEX_BOOL) || (t1 != LEX_BOOL && t2 == LEX_BOOL)){ 
		throw SemErr("wrong types in operation");
	}

	if ( (op==LEX_PLUS || op==LEX_MINUS || op==LEX_TIMES || op==LEX_SLASH) &&  t != LEX_BOOL ){
		
 		st_lex.push(t);
	} else if ( op == LEX_MOD && t == LEX_INT){
		st_lex.push(t);
	} else if ( (op == LEX_OR || op == LEX_AND)  && t == LEX_BOOL){
		st_lex.push(t);
	} else if ( (op == LEX_EQ || op == LEX_NEQ || op == LEX_GEQ || op == LEX_LEQ || op == LEX_GTR || op == LEX_LSS)){
		t = LEX_BOOL;
		st_lex.push(t);
	}

	else 
		throw SemErr("wrong types in operations");
	if ((op == LEX_SLASH && t == LEX_INT) || op != LEX_SLASH){
		prog.put_lex (op);
	} else if (t == LEX_FLOAT){
		prog.put_lex(LEX_FSLASH);
	}
}

void Parser::analyze (){
	gl ();
	P ();


	prog.print();
	cout << endl << "Yes" << endl;
	}
void Parser::P (){
	if ( c_type == LEX_PROGRAM ){

		gl ();
	}else
		throw SyntaxErr(curr_lex);
	D1 ();
	B ();


	if ( c_type != LEX_FIN )
		throw SyntaxErr(curr_lex);
	}
	
void Parser::D1 (){
	if ( c_type == LEX_VAR ){
		gl ();
		D ();
		while ( c_type == LEX_SEMICOLON ){
			gl();
			D();
		}
	} else
		throw SyntaxErr(curr_lex);
}

void Parser::D (){

	type_of_lex classify;
	//st_int.reset();
	if ( c_type == LEX_INT){
		gl();
		classify = LEX_INT;
	} else if (c_type == LEX_BOOL){
		classify = LEX_BOOL;
		gl();
	} else if (c_type == LEX_FLOAT){
		classify = LEX_FLOAT;
		gl();
	} else if (c_type == LEX_LFIGPAR){
		return;
	} else {
		throw SyntaxErr(curr_lex);
	}
	
	if (c_type == LEX_ID){
		if (TID[c_val].get_declare() != 0){
			throw SemErr("twice");
		} else {
			TID[c_val].put_type(classify);
			TID[c_val].put_declare();
		}
		gl();
	} else 
		throw SyntaxErr(curr_lex);
		
	while ( c_type == LEX_COMMA ){
						
		gl();
		if (c_type != LEX_ID)
			throw SyntaxErr(curr_lex);
		else {
			if (TID[c_val].get_declare() != 0){
				throw SemErr("twice");
			} else {

				TID[c_val].put_type(classify);
				TID[c_val].put_declare();
			}
			//st_int.push ( c_val ); 
			gl();
		}
	}
}

	

void Parser::continue_break(){

	try{
		if (c_type == LEX_CONTIN){
			int plcont = st_int.pop();
			st_int.push(plcont);
			prog.put_lex(Lex(POLIZ_LABEL, plcont));
			prog.put_lex(Lex(POLIZ_GO));
			gl();
		} 
		if (c_type == LEX_BREAK){
				list  *plbreak = st_break.pop();
				st_break.push(plbreak);
				plbreak->add( prog.get_free());
				prog.blank();
				prog.put_lex(Lex(POLIZ_GO));
				gl();
		
		}
	} catch (runtime_error e){
		if (string(e.what()).compare("Stack_is_empty") == 0){
			throw SemErr("continue or break in non loop");
		}
	}
	catch(...){
		throw;
	}
}

void   Parser::B (){




	if ( c_type == LEX_LFIGPAR ){
		gl();
		if (c_type == LEX_CONTIN || c_type == LEX_BREAK){
			continue_break();
		}
		S();
		while ( c_type == LEX_SEMICOLON ){

			gl();
			if (c_type == LEX_CONTIN || c_type == LEX_BREAK){
				continue_break();
			}
			S();
		}
		if (c_type == LEX_CONTIN || c_type == LEX_BREAK){
			continue_break();
		}
		if ( c_type == LEX_RFIGPAR )
			gl();
		else
			throw SyntaxErr(curr_lex);
	} else
		throw SyntaxErr(curr_lex);


}


void Parser::S () {
	list * plbreak = new list;;
	int pl0, pl1, pl2, pl3;
	if ( c_type == LEX_IF ){

		gl();

		if (c_type == LEX_LPAREN){
			gl();

			E();
			eq_bool();
		}else 
			throw SyntaxErr(curr_lex);

	pl2 = prog.get_free ();
	prog.blank();
	prog.put_lex (Lex(POLIZ_FGO));
		if ( c_type == LEX_RPAREN ){
			gl();

			B();
		} else
			throw SyntaxErr(curr_lex);

			prog.put_lex(Lex(POLIZ_LABEL,prog.get_free()),pl2);
		if (c_type == LEX_ELSE){
			pl3 = prog.get_free();
			prog.blank();
			prog.put_lex (Lex(POLIZ_GO));
			prog.put_lex(Lex(POLIZ_LABEL,prog.get_free()),pl2);

			gl();
			B();
		prog.put_lex(Lex(POLIZ_LABEL,prog.get_free()),pl3);
		} 


	} else if ( c_type == LEX_WHILE ){
		st_break.push(plbreak);

		pl0 = prog.get_free();
		st_int.push(pl0);
		gl();
		if (c_type == LEX_LPAREN){
			gl();
			E();
			eq_bool();
		} else {
			throw SyntaxErr(curr_lex);
		}
		
		if (c_type == LEX_RPAREN){
			pl1 = prog.get_free();
			prog.blank();
			prog.put_lex (Lex(POLIZ_FGO));
			gl();
			B();
			

		} else {
			throw SyntaxErr(curr_lex);
		}



		prog.put_lex (Lex (POLIZ_LABEL, pl0));

		prog.put_lex (Lex ( POLIZ_GO));

		prog.put_lex (Lex(POLIZ_LABEL, prog.get_free()),pl1);

		plbreak = st_break.pop();

		while ((plbreak)->empty() == 0){
			prog.put_lex(Lex(POLIZ_LABEL, prog.get_free()), (plbreak)->get());
		}
		free(plbreak);
		st_int.pop();
	}  else // end while 
	if ( c_type == LEX_READ ) {


		gl();
		if ( c_type == LEX_LPAREN ){
			gl();

			if ( c_type == LEX_ID ){
				check_id_in_read();
				prog.put_lex (Lex ( POLIZ_ADDRESS, c_val) );

				gl();
			} else
				throw SyntaxErr(curr_lex);
			if ( c_type == LEX_RPAREN ){
				gl();
				prog.put_lex (Lex (LEX_READ));
			} else
				throw SyntaxErr(curr_lex);
		} else
			throw SyntaxErr(curr_lex);
	} else if ( c_type == LEX_WRITE ){ //end read
	
		
	
		gl();
		if ( c_type == LEX_LPAREN ) {
			gl();
			E();
			if ( c_type == LEX_RPAREN ) {
				gl();
				prog.put_lex (Lex(LEX_WRITE));
			} else
				throw SyntaxErr(curr_lex);
		} else
			throw SyntaxErr(curr_lex);
	} else if ( c_type == LEX_ID ) {  //end write

		check_id (1);

		prog.put_lex (Lex(POLIZ_ADDRESS,c_val));
		gl();
		if ( c_type == LEX_ASSIGN ){

			gl();
			E(); 
			eq_type();
			prog.put_lex (Lex (LEX_ASSIGN) );

		} else if (c_type == LEX_UNPLUS){
			check_plus_minus();
			prog.put_lex(LEX_PSPLUS);
			gl();
		} else if (c_type == LEX_UNMIN){
			check_plus_minus();
			prog.put_lex(LEX_PSMIN);
			gl();
		} else {
			throw SyntaxErr(curr_lex);
		
		}
	} else if (c_type == LEX_FOR){ //assign-end
		st_break.push(plbreak);
 		
		gl();
		if (c_type == LEX_LPAREN){
			gl();
			if (c_type != LEX_SEMICOLON)
				S();
			if (c_type == LEX_SEMICOLON) {
				pl0 = prog.get_free();
				gl();
				E();
				eq_bool();
				pl1 = prog.get_free();
				prog.blank();
				prog.put_lex(POLIZ_FGO);
				pl2 = prog.get_free();
				prog.blank();
				prog.put_lex(POLIZ_GO);


				if (c_type == LEX_SEMICOLON){
					pl3 = prog.get_free();
					st_int.push(pl3);
					gl();
			

					if (c_type != LEX_RPAREN)
						S();
					prog.put_lex(Lex(POLIZ_LABEL, pl0));
					if (c_type == LEX_RPAREN){
						gl();
						prog.put_lex(Lex(POLIZ_LABEL, prog.get_free()), pl2);
						B();
						prog.put_lex(Lex(POLIZ_LABEL, pl3));
						prog.put_lex(Lex(POLIZ_LABEL, prog.get_free()), pl1);
						plbreak = st_break.pop();
						while ((plbreak)->empty() == 0){
							prog.put_lex(Lex(POLIZ_LABEL, prog.get_free()), (plbreak)->get());
						}
						free(plbreak);
						st_int.pop();
					} else throw SyntaxErr(curr_lex);
				} else throw SyntaxErr(curr_lex);
			} else throw SyntaxErr(curr_lex);
		} else throw SyntaxErr(curr_lex);
	

	} else if (c_type == LEX_UNMIN){

		gl();
		if (c_type = LEX_ID){
			check_id();
			prog.put_lex (Lex(POLIZ_ADDRESS,c_val));
			gl();
		} else {
			throw SyntaxErr(curr_lex);
		}
		check_plus_minus();
		prog.put_lex(LEX_PSMIN);
	} else if (c_type == LEX_UNPLUS){
		gl();
				if (c_type = LEX_ID){
			check_id();
			prog.put_lex (Lex(POLIZ_ADDRESS,c_val));
			gl();
		} else {
			throw SyntaxErr(curr_lex);
		}
		check_plus_minus();
		prog.put_lex(LEX_PSPLUS);
	} 
}
	

void Parser::E () {
	E1();

	if ( c_type == LEX_EQ || c_type == LEX_LSS || c_type == LEX_GTR ||
	c_type == LEX_LEQ || c_type == LEX_GEQ || c_type == LEX_NEQ ) {
		st_lex.push (c_type);
		gl();
		E1();
		check_op();
	}
}



void Parser::E1 (){
	T();
	while ( c_type==LEX_PLUS || c_type==LEX_MINUS || c_type==LEX_OR ) {
		st_lex.push (c_type);
		gl();
		T();
		check_op();
	}
}
	
void Parser::T (){
	F(); 
	while ( c_type==LEX_TIMES || c_type==LEX_SLASH || c_type==LEX_AND || c_type == LEX_MOD ) {
		st_lex.push (c_type);
		gl();
		F();
		check_op();
	}
}
	

void Parser::F () {
	if ( c_type == LEX_ID ) {
		
		check_id();
		//cout << curr_lex << "//////" << Lex(LEX_ID, c_val)<<endl;
		prog.put_lex (Lex (LEX_ID, c_val));

		gl();

	} else if ( c_type == LEX_NUM ) {
		st_lex.push ( LEX_INT );
		prog.put_lex ( curr_lex );
		gl();

	}else if (c_type == LEX_UNPLUS){
			check_plus_minus();
			//printf("AAAAAAA\n");
			gl();
			F();
			prog.put_lex(LEX_PRPLUS);
	} else if (c_type == LEX_UNMIN){
			check_plus_minus();
			gl();
			F();
			prog.put_lex(LEX_PRMIN);
	
	} else if ( c_type == LEX_TRUE ) {
		st_lex.push ( LEX_BOOL );
		//cout<< "TRUE" << curr_lex<<endl;
		prog.put_lex (Lex (LEX_TRUE, 1) );
		gl();
	} else if ( c_type == LEX_FALSE ) {
		st_lex.push ( LEX_BOOL );
		prog.put_lex (Lex (LEX_FALSE, 0) );
		gl();
	}else if (c_type == LEX_NUMFLOAT){
		st_lex.push(LEX_FLOAT);
		prog.put_lex(curr_lex);
		gl();
	} else if ( c_type == LEX_NOT ) {
		gl();
		F();
		check_not();
	} else if ( c_type == LEX_LPAREN ){
		gl();
		E();
		if ( c_type == LEX_RPAREN)
			gl();
		else
			throw SyntaxErr(curr_lex);

	} else {
		throw SyntaxErr(curr_lex);
	}

	if (c_type == LEX_UNPLUS || c_type == LEX_UNMIN){
		check_plus_minus();
		addpoliz_post_plus_minus();
		gl();
	}
}





