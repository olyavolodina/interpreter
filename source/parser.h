#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <exception>
#include "type_of_lex.h"
#include "scanner.h"
#include "Poliz.h"
#include "list.h"

#ifndef Parser_h
#define Parser_h



template <class T, int max_size > class Stack{
	T s[max_size];
	int top;
public :
	Stack(){top = 0;}
	void reset() { top = 0; }
	void push(T i);
	T pop();
	bool is_empty(){ return top == 0; }
	bool is_full() { return top == max_size; }
	};
	
template <class T, int max_size >
	void Stack <T, max_size >::push(T i){
	if ( !is_full() ){

		s[top] = i;
		++top;
	} else
		throw runtime_error("Stack_is_full");
	}

template <class T, int max_size >
	T Stack <T, max_size >::pop(){
	if ( !is_empty() ){

		--top;
		return s[top];
	} else
		throw runtime_error("Stack_is_empty");
	}

class Parser{
	Lex curr_lex, prev_lex; // текущая лексема
	type_of_lex c_type;
	int c_val;
	Scanner scan;
	Stack < int , 100 > st_int; // for continue points
	Stack < list* , 100 > st_break;
	Stack < type_of_lex, 100 > st_lex;
	void P(); // процедуры РС-метода
	void D1();
	void D();
	void B();
	//void B1(); // int - beginning of loop
	void S();
	void E();
	void E1();
	void continue_break();
	void T();
	void F();
	void check_id (int);
	void check_op ();
	void addpoliz_post_plus_minus();
	void addpoliz_pred_minus_plus();

	void check_not ();
	void check_plus_minus ();
	void eq_type ();
	void eq_bool ();
	void check_id_in_read ();
	void gl (){ // получить очередную лексему

		prev_lex = curr_lex;
		curr_lex = scan.get_lex();

		c_type = curr_lex.get_type();
		c_val = curr_lex.get_value();
	}
public :
	Poliz prog; // внутреннее представление программы
	Parser ( const char *program) : scan (program) {};
	void analyze(); // анализатор с действиями
};
#endif