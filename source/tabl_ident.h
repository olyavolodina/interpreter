#include <iostream>
#include <cstdio>
#include <cstring>
#include <cstdlib>
#include "type_of_lex.h"
#ifndef ident_h
#define ident_h
using namespace std;

class ident{
	char * name;
	bool declare;
	type_of_lex type;
	bool assign;
	float value;
public :
	ident (){
		declare = false ;
		assign = false ;
	}
	char *get_name (){
 		return name;
	}
	void put_name ( const char *n){
		name = new char [ strlen(n) + 1 ];
		strcpy ( name, n );
	}
	bool get_declare (){
		return declare;
	}
	void put_declare (){
		declare = true;
	}
	type_of_lex get_type (){
		return type;
	}
	void put_type ( type_of_lex t ){
		type = t;
	}
	bool get_assign (){
		return assign;
	}
	void put_assign (){
		assign = true ;
	}
	float get_value (){
				//cout<< value<< "VALUE " << endl;
		return value;
	}
	void put_value ( float v){
		value = v;

	}
};

class tabl_ident{
	ident * p;
	int size;
	int top;
public :
	tabl_ident ( int max_size ){
		p = new ident[size=max_size];
		top = 1;
	}
	~tabl_ident (){
		delete []p;
	}
	ident & operator [] ( int k ){
		return p[k];
	}
	int put ( const char *buf );
};
#endif