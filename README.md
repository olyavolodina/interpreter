Лексический, синтаксический, семантический анализ, полиз и экзекутер



make test - запускает программу, выводит Yes при правильном построении 

make clean - удаляет объектные и исполнимые файлы

Есть класс ошибок, который выводит ошибку и указывает строку и столбец, где она произошла
prog - выводит i-ое число фибоначчи
prog2  - сумму чисел до i
prog3 - что-то с float
prog4 - печатает 1, если число простое, 0 - если составное


Грамматика :
P -> program D1 B 
D1 -> var D <,D>
D -> [int | float | bool] name <; name>
B -> {S <;S>}
S -> name = E | if (E) B <else B> | while (E) B |  read (I) | write (E) | for (S|epsilon ; E; S| epsilon) B  | name ++ | name --| ++name|| --name || epsilon


E -> E1 [ == | > | < | != | <= | >=] E 1 | E 1
E 1 -> T <[ + | - | "||" ] T>
T -> F <[ * | / | && | %] F>
F -> true | false| name| number| not F | --F| ++F| F--| F++| (E)
